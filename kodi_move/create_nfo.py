#!/usr/bin/env python
from __future__ import annotations
from typing import (
    Optional,
    Iterator,
    Any,
    Iterable,
)
from dataclasses import dataclass
import os
import sys
from pathlib import Path
import argparse
import subprocess
import json
from xml.sax.saxutils import escape
import shlex


def argparser():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("folder", type=Path, help="search folder")
    parser.add_argument("--no-recursive", dest="recursive",
                        action="store_false", help="do not search recursively")
    parser.add_argument("--filter", type=str, default="*.*", help="file filter")
    parser.add_argument("--force", action="store_true", help="update .nfo if it exists")
    parser.add_argument("--tags", type=str, nargs="*", help="movie tags")
    parser.add_argument("--actors", type=str, nargs="*", help="actors")
    return parser


def walk(args) -> Iterator[Path]:
    if not args.recursive or not args.folder.is_dir():
        yield args.folder
    else:
        for path, _, __ in os.walk(args.folder):
            yield Path(path)


def create_tag(tag: str):
    return f"<tag>{escape(tag)}</tag>"


def create_actor(name: str):
    return f'<actor><name>{escape(name)}</name></actor>'


@dataclass
class MediaInfo:
    ref: Path
    title: str
    video: int
    audio: int
    text: int

    @classmethod
    def parse(cls, data: dict[str, Any]) -> 'MediaInfo':
        media = data["media"]
        tracks = media["track"]
        general = tracks[0]
        return cls(
            ref=Path(media["@ref"]),
            title=general.get("Title"),
            video=int(general.get("VideoCount", 0)),
            audio=int(general.get("AudioCount", 0)),
            text=int(general.get("TextCount", 0)),
        )

    def get_title(self):
        return self.title or self.ref.stem

    def is_video(self):
        return self.video

    def is_audio(self):
        return not self.is_video() and self.audio

    def is_text(self):
        return not self.is_audio() and self.text

    def is_media(self):
        return self.video or self.audio

    def to_nfo(self, tags: Iterable[str], actors: Iterable[str]) -> Optional[str]:
        if not self.is_media():
            return None
        tags = "\n".join(create_tag(x) for x in tags)
        actors = "\n".join(create_actor(x) for x in actors)
        return f'''
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<movie>
    <title>{escape(self.get_title())}</title>
    {tags}{actors}
</movie>
        '''


def explore_folder(folder: Path, args) -> int:
    if folder.is_dir():
        path = f"{shlex.quote(str(folder))}{os.path.sep}{args.filter}"
    else:
        path = shlex.quote(str(folder))

    ret = subprocess.run(
        f"mediainfo --Output=JSON {path}",
        capture_output=True, shell=True, check=False
    )

    if ret.returncode != 0:
        return 0

    data = json.loads(ret.stdout)

    if isinstance(data, dict):
        data = [data]

    elements = 0
    for item in data:
        data = MediaInfo.parse(item)
        if not data.is_video():
            continue
        nfo = data.to_nfo(args.tags or [], args.actors or [])
        if not nfo:
            continue
        dest = data.ref.with_suffix(".nfo")
        if dest.exists() and not args.force:
            continue
        with open(dest, "w", encoding="utf-8") as fdnfo:
            fdnfo.write(nfo)
            elements += 1
    return elements


def command(args):
    for directory in walk(args):
        # glob mediainfo
        print(directory, file=sys.stderr, end="...")
        videos = explore_folder(directory, args)
        print(videos, "videos", file=sys.stderr)


def main():
    command(argparser().parse_args())


if __name__ == "__main__":
    main()
