#!/usr/bin/env python
from typing import (
    Tuple,
    Iterator,
)

import os
import sys
import sqlite3
import argparse
from pathlib import Path
from enum import Enum


def argparser():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--kodi-path", type=Path, default=Path("~/.kodi/userdata/Database/"))
    parser.add_argument("oldprefix", type=Path, help="old path prefix")
    parser.add_argument("newprefix", type=Path, help="new path prefix")
    parser.add_argument("folder", type=Path, nargs="?", help="folder to move")
    return parser


def normalize_prefix(oldprefix: Path, newprefix: Path) -> Tuple[str, str]:
    " normalize prefixes "
    str_old = str(oldprefix)
    str_new = str(newprefix)

    old_sep = str_old.endswith(os.path.sep)
    new_sep = str_new.endswith(os.path.sep)

    if old_sep or new_sep:
        if not old_sep:
            str_old = str_old + os.path.sep
        if not new_sep:
            str_new = str_new + os.path.sep

    return str_old, str_new


def check_table_exists(connection: sqlite3.Connection, table_name: str) -> bool:
    " return True if the table exists "
    cursor =connection.execute(
        "SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name=? LIMIT 1",
        (table_name,)
    )

    return cursor.fetchone()[0] > 0


class DatabaseType(Enum):
    " database type "
    VIDEO = 1
    MUSIC = 2
    TEXTURE = 3


def get_db_type(db_name: Path):
    " database type "
    with sqlite3.connect(db_name) as con:
        # get movie
        if check_table_exists(con, "movie"):
            return DatabaseType.VIDEO
        # get album
        if check_table_exists(con, "album"):
            return DatabaseType.MUSIC
        # get texture
        if check_table_exists(con , "texture"):
            return DatabaseType.TEXTURE

    return None


def print_if_any(table: str, rowcount: int):
    " print if there are affected rows "
    if not rowcount:
        return
    print("\t%s: %d row(s)" % (table, rowcount), file=sys.stderr)


def update(table: str, field: str, oldprefix: str, newprefix: str) -> Tuple[str, dict]:
    " SQL update "
    return ("""
        UPDATE {table}
        SET {field} = :newprefix || SUBSTR({field}, LENGTH(:oldprefix) + 1)
        WHERE {field} LIKE :oldprefix || '%' """.format(table=table, field=field),
        {
            "newprefix": newprefix,
            "oldprefix": oldprefix,
        }
    )


def update_path(connection: sqlite3.Connection, oldprefix: str, newprefix: str) -> int:
    " SQL update table 'path'"
    cursor = connection.execute(*update("path", "strPath", oldprefix, newprefix))

    return cursor.rowcount


def move_videos(db_name: Path, oldprefix: Path, newprefix: Path):
    """
    path strPath
    movie c22
    episode c18
    art url
    tvshow c16
    files strFilename
    """
    str_oldprefix, str_newprefix = normalize_prefix(oldprefix, newprefix)
    with sqlite3.connect(db_name) as con:
        # path
        print_if_any("path", update_path(con, str_oldprefix, str_newprefix))
        # movie
        cursor = con.execute(*update("movie", "c22", str_oldprefix, str_newprefix))
        print_if_any("movie", cursor.rowcount)
        # episode
        cursor = con.execute(*update("episode", "c18", str_oldprefix, str_newprefix))
        print_if_any("episode", cursor.rowcount)
        # art
        cursor = con.execute(*update("art", "url", str_oldprefix, str_newprefix))
        print_if_any("art", cursor.rowcount)
        # tvshow
        cursor = con.execute(*update("tvshow", "c16", str_oldprefix, str_newprefix))
        print_if_any("tvshow", cursor.rowcount)
        # files
        cursor = con.execute(*update("files", "strFilename", str_oldprefix, str_newprefix))
        print_if_any("files", cursor.rowcount)

        con.commit()


def move_music(db_name: Path, oldprefix: Path, newprefix: Path):
    """
    path strPath
    """
    str_oldprefix, str_newprefix = normalize_prefix(oldprefix, newprefix)
    with sqlite3.connect(db_name) as con:
        print_if_any("path", update_path(con, str_oldprefix, str_newprefix))

        con.commit()


def move_texture(db_name: Path, oldprefix: Path, newprefix: Path):
    """
    texture.db

    UPDATE path SET url = REPLACE(url,'smb://my_nas/old_share', 'smb://my_nas/new_share');
    UPDATE path SET texture = REPLACE(texture,'smb://my_nas/old_share', 'smb://my_nas/new_share');
    """
    str_oldprefix, str_newprefix = normalize_prefix(oldprefix, newprefix)
    with sqlite3.connect(db_name) as con:
        # tvshow
        cursor = con.execute(*update("path", "url", str_oldprefix, str_newprefix))
        print_if_any("tvshow", cursor.rowcount)
        # files
        cursor = con.execute(*update("path", "texture", str_oldprefix, str_newprefix))
        print_if_any("files", cursor.rowcount)

        con.commit()


def db_move(database: Path, oldprefix: Path, newprefix: Path):
    " move location "
    assert newprefix.exists()

    # update video
    db_type = get_db_type(database)

    if db_type == DatabaseType.VIDEO:
        move_videos(database, oldprefix, newprefix)
    elif db_type == DatabaseType.MUSIC:
        move_music(database, oldprefix, newprefix)
    elif db_type == DatabaseType.TEXTURE:
        move_texture(database, oldprefix, newprefix)
    else:
        return


def iter_databases(kodi_path: Path) -> Iterator[Path]:
    " iterate potentially affected databases"
    yield from sorted(kodi_path.glob("MyVideos*.db"))
    yield from sorted(kodi_path.glob("MyMusic*.db"))
    yield from sorted(kodi_path.glob("Textures*.db"))


def move(args):
    " move kodi collection paths "

    newprefix = args.newprefix
    oldprefix = args.oldprefix
    if args.folder:
        newprefix = newprefix / args.folder
        oldprefix = oldprefix / args.folder

    assert args.newprefix.exists()
    kodi_path: Path = args.kodi_path.expanduser()

    for database in iter_databases(kodi_path):
        print("DATABASE: %s" % database, file=sys.stderr)
        db_move(database, oldprefix, newprefix)


def main():
    "cli command"
    move(argparser().parse_args())


if __name__ == "__main__":
    main()
