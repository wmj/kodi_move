#!/usr/bin/env python

import setuptools

setuptools.setup(
    name="kodi_move",
    version="0.0.1",
    packages=["kodi_move"],
    python_requires=">=3.6",
)
